import json
import requests
import re
from tools import sJSON


# 获取地理位置信息
ipurl = "http://2017.ip138.com/ic.asp"
ip_add = requests.get(ipurl).text
theIP = re.findall(r"\d{1,3}\.\d{1,3}\.\d{1,3}.\d{1,3}",ip_add)

#秘钥
key = '3DU75EPOWE'
url = 'https://api.seniverse.com/v3/weather/now.json?key='+key+'&location='+theIP[0]+'&language=zh-Hans&unit=c'

weather = requests.get(url)
return_weather = weather.text

print(sJSON.JSONTools.formatJSON(return_weather,2))

j_str = json.loads(return_weather)
print(j_str.get('results')[0].get('location').get('path')+'的天气是:'+j_str.get('results')[0].get('now').get('text')+",温度是:"+j_str.get('results')[0].get('now').get('temperature')+"℃,天气更新时间是:"+j_str.get('results')[0].get('last_update'))