# 解析json的方法包
import json


class JSONTools:
    """
            拆解内部json
            参数说明:list_data  要解包的数据
                     dic  封装的位置
            返回一个数组list集合
        """
    def ergodic_JSON(list_data:str, dic):
        temp_dic = {}
        newstr = json.loads(list_data)
        for key in newstr:
            if type(newstr.get(key)) == list:
                temp_dic = newstr.get(key)
            else:
                dic[key] = newstr.get(key)
        return temp_dic


    """格式化json数据,如果此json中的字段存在重复可能导致解析失败
        参数说明:str  json数据
                 num  层数,最外层也算在内
        返回一个层级的json数据
    """
    def formatJSON(strinfo,num):
        # 声明一个字典
        dic = {} #返回主参数字典
        temp = {}#临时字典
        newstr = json.loads(strinfo)
        for key in newstr:
            temp_key = newstr.get(key) #临时数据
            if num == 1:
                # 表示一个层级
                dic[key] = temp_key
            else:
                #表示多层,大于2层的时候
                if type(temp_key) == list:
                    #多层中嵌套层
                    for shuzu in temp_key:
                        if type(shuzu) == list:
                            temp_dic = JSONTools.ergodic_JSON(str(shuzu),dic)
                            for i in range(num - 2):
                                temp_dic = JSONTools.ergodic_JSON(str(temp_dic),dic)
                        else:
                            dic[shuzu] = shuzu
                else:
                    # 多层中的单层数据
                    dic[key] = temp_key
        return dic